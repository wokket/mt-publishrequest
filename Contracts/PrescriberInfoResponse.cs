﻿using MassTransit;
using System;

namespace Contracts
{
	public class PrescriberInfoResponse: CorrelatedBy<Guid>
	{
		/// <summary>
		/// Who made this reply
		/// </summary>
		public string SourceApplication { get; set; }

		public string UserName { get; set; }

		/// <summary>
		/// The users prescriber number
		/// </summary>
		public string PrescriberNumber { get; set; }

		public Guid CorrelationId { get; set; }
	}
}
