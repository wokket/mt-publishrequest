﻿using MassTransit;
using System;

namespace Contracts
{
	public class IAmPrescriberRequest : CorrelatedBy<Guid>
	{
		/// <summary>
		/// How long is this request valid for? Replies after this date will be ignored.
		/// </summary>
		public DateTime ValidUntilUtc { get; set; }

		// Criteria of the guy we're reuesting info about
		public string UserName { get; set; }
		public string ActualName { get; set; }
		public string EmailAddress { get; set; }

		public Guid CorrelationId { get; set; }
	}
}
