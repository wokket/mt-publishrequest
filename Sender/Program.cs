﻿using Contracts;
using MassTransit;
using MassTransit.Context;
using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Sender
{
	static class Program
	{
		static void Main(string[] args)
		{
			AsyncContext.Run(() => MainAsync(args));
		}

		static Dictionary<Guid, Tuple<User, Request<IAmPrescriberRequest>>> _pendingRequests = new Dictionary<Guid, Tuple<User, Request<IAmPrescriberRequest>>>();
		static IBusControl _bus;

		static async void MainAsync(string[] args)
		{

			_bus = CreateBus();
			_bus.Start();

			Console.WriteLine("Press any key to send requests...");
			Console.ReadKey();


			//imagine we get a request for a new user
			var user = new User() { UserName = "User1" };
			await MakePrescriberRequest(user);


			//and another one
			user = new User() { UserName = "User2" };
			await MakePrescriberRequest(user);

			//and again
			user = new User() { UserName = "User3" };
			await MakePrescriberRequest(user);


			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();


			_bus.Stop();
		}


		static TimeSpan RequestTimeout = TimeSpan.FromSeconds(1);
		private static async Task MakePrescriberRequest(User user)
		{
			
			var requestMessage = new IAmPrescriberRequest()
			{
				UserName = user.UserName,
				ValidUntilUtc = DateTime.UtcNow.Add(RequestTimeout),
				CorrelationId = Guid.NewGuid() //used to map the responses
			};

			var request = await _bus.PublishRequest(requestMessage, (ctx) =>
			{
				ctx.Timeout = RequestTimeout;
				//ctx.Watch<PrescriberInfoResponse>(ReplyReceived); //this lets us gather a whole swag of messages, but never 'completes' the request
				ctx.Handle<PrescriberInfoResponse>(ReplyReceived); //first one to reply wins a prize and closes the competition.
			});

			var typed = (SendRequest<IAmPrescriberRequest>)request;
			

			_pendingRequests.Add(requestMessage.CorrelationId, new Tuple<User, Request<IAmPrescriberRequest>>(user, request));

			Log($"Published Request for {user.UserName}");

			try
			{
				await request.Task; //.WithCancellation(cts.Token);
				Log($"{user.UserName} request complete...");
			}
			catch (RequestTimeoutException)
			{
				Log($"{user.UserName} request timed out at request...");
			}
			finally
			{
				_pendingRequests.Remove(requestMessage.CorrelationId); //finished
			}

		}

		private static Task ReplyReceived(ConsumeContext<PrescriberInfoResponse> ctx)
		{

			if (!_pendingRequests.ContainsKey(ctx.Message.CorrelationId))
			{
				Log($"Received late response for {ctx.Message.CorrelationId}");
				return Task.FromResult(true);
			}

			var key = ctx.Message.CorrelationId;
			var data = _pendingRequests[key];
			var user = data.Item1;

			Log($"Received reply from {ctx.Message.SourceApplication} for {user.UserName}:  Prescriber#={ctx.Message.PrescriberNumber}");

			if (ctx.Message.PrescriberNumber?.Length > 0) //found the user, and got his prescriber #
			{
				user.PrescriberNumber = ctx.Message.PrescriberNumber;
				var request = data.Item2;

			}

			//save the user etc
			return Task.FromResult(true);
		}

		private static void Log(string output)
		{
			Console.WriteLine($"{DateTime.Now}: {output}");
		}

		private static IBusControl CreateBus()
		{
			return Bus.Factory.CreateUsingRabbitMq(cfg =>
			{
				var host = cfg.Host(new Uri("rabbitmq://localhost"), h =>
				{
					h.Username("service");
					h.Password("service");
				});
			});
		}


		//from http://blogs.msdn.com/b/pfxteam/archive/2012/10/05/how-do-i-cancel-non-cancelable-async-operations.aspx
		public static async Task WithCancellation(this Task task, CancellationToken cancellationToken)
		{
			var tcs = new TaskCompletionSource();
			using (cancellationToken.Register(s => ((TaskCompletionSource)s).TrySetResult(), tcs))
			{
				if (task != await Task.WhenAny(task, tcs.Task))
				{
					throw new OperationCanceledException(cancellationToken);
				}

				await task;
			}
		}
	}
}
