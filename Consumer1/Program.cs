﻿using Contracts;
using MassTransit;
using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consumer1
{
	class Program
	{
		static void Main(string[] args)
		{
			AsyncContext.Run(() => MainAsync(args));

		}

		//fake database
		static User[] _users = new User[] {
			new User {UserName = "User1", PrescriberNumber="P123456" },
			new User {UserName = "User2", PrescriberNumber="P987653" }
		};

		static IBusControl _bus;

		static async void MainAsync(string[] args)
		{

			_bus = CreateBus();
			await _bus.StartAsync();

			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();


			await _bus.StopAsync();
		}

		private static async Task MessageReceived(ConsumeContext<IAmPrescriberRequest> ctx)
		{
			var msg = ctx.Message;
			await Task.Delay(500 + new Random().Next(300));

			Log($"Received request for {msg.UserName}...");

			var user = _users.FirstOrDefault(u => u.UserName == msg.UserName);

			if (user == null)
			{
				return; //nothing to do
			}

			var reply = new PrescriberInfoResponse
			{
				CorrelationId = msg.CorrelationId,
				UserName = user.UserName,
				PrescriberNumber = user.PrescriberNumber,
				SourceApplication = "Consumer1"
			};

			await ctx.RespondAsync(reply);
		}


		private static void Log(string output)
		{
			Console.WriteLine($"{DateTime.Now}: {output}");
		}

		private static IBusControl CreateBus()
		{
			return Bus.Factory.CreateUsingRabbitMq(cfg =>
			{
				var host = cfg.Host(new Uri("rabbitmq://localhost"), h =>
				{
					h.Username("service");
					h.Password("service");
				});

				cfg.ReceiveEndpoint(host, ep =>
				{
					ep.Handler<IAmPrescriberRequest>(MessageReceived);
					ep.PrefetchCount = 1;
					ep.UseConcurrencyLimit(1);
				});
			});
		}
	}
}
